﻿using NUnit.Framework;

namespace OnlineStoreAutomationTestPageModel
{
    public class TestAddToCart : MainTest
    {
        private string _productName = "Faded Short Sleeve T-shirts";

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartWithinTheProductDetails_THEN_AddToCart()
        {

            StoreSetup.MainPage.SearchField.SendKeys(_productName);
            StoreSetup.MainPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailsPage.AddToCartButton.Click();
            StoreSetup.ProductDetailsPage.ProceedToCheckoutButton.Click();

            Assert.IsTrue(StoreSetup.CartPage.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartFromSearchResult_THEN_AddToCart()
        {
            StoreSetup.MainPage.SearchField.SendKeys(_productName);
            StoreSetup.MainPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductHover.Perform();
            StoreSetup.SearchPage.AddToCartButton.Click();
            StoreSetup.ProductDetailsPage.ProceedToCheckoutButton.Click();

            Assert.IsTrue(StoreSetup.CartPage.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GIVEN_ProductToAddInTheCart_WHEN_RemovingProductInTheCart_THEN_RemoveProduct()
        {

            StoreSetup.MainPage.SearchField.SendKeys(_productName);
            StoreSetup.MainPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailsPage.AddToCartButton.Click();
            StoreSetup.ProductDetailsPage.ProceedToCheckoutButton.Click();
            StoreSetup.CartPage.RemoveProduct.Click();
            StoreSetup.CartPage.WaitForUpdate();

            Assert.AreEqual("(empty)", StoreSetup.CartPage.ProductCounter.GetAttribute("textContent"));
        }
    }
}

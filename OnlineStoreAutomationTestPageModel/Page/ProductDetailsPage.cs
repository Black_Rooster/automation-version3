﻿using OpenQA.Selenium;

namespace OnlineStoreAutomationTestPageModel.Page
{
    public class ProductDetailsPage : MainPage
    {
        public ProductDetailsPage(IWebDriver driver) : base(driver)
        {

        }

        public IWebElement SendToFriendLink => _driver.FindElement(By.Id("send_friend_button"));
        public IWebElement AddToCartButton => _driver.FindElement(By.Name("Submit"));
        public IWebElement ProceedToCheckoutButton => _driver.FindElement(By.XPath("//a[@title='Proceed to checkout']"));
        public IWebElement ContinueShoppingButton => _driver.FindElement(By.XPath("//span[@title='Continue shopping']"));
        public IWebElement FriendNameInput => _driver.FindElement(By.Id("friend_name"));
        public IWebElement FriendEmailInput => _driver.FindElement(By.Id("friend_email"));
        public IWebElement SendEmailButton => _driver.FindElement(By.Id("sendEmail"));
        public IWebElement ErrorMessage => _driver.FindElement(By.Id("send_friend_form_error"));
        public IWebElement SuccessModal => _driver.FindElement(By.XPath("//div[@class='fancybox-wrap fancybox-desktop fancybox-type-html fancybox-opened']"));

        public void WaitForAddToCartModal()
        {
            _driverWait.Until(driver => driver.FindElement(By.Id("layer_cart")).GetCssValue("display") == "block");
        }

    }
}

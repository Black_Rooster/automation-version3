﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace OnlineStoreAutomationTestPageModel.Page
{
    public class SearchPage : MainPage
    {
        public SearchPage(IWebDriver driver) : base(driver)
        {
        }


        public IWebElement ProductLink => _driver.FindElement(By.XPath("//a[@title='Faded Short Sleeve T-shirts' and @class='product-name']"));
        public IAction ProductHover => action.MoveToElement(ProductLink).Build();
        public IWebElement AddToCartButton => ProductLink.FindElement(By.XPath("//a[@title='Add to cart']"));

    }
}

﻿using OpenQA.Selenium;

namespace OnlineStoreAutomationTestPageModel.Page
{
    public class CartPage : MainPage
    {
        public CartPage(IWebDriver driver) : base(driver)
        {

        }

        public IWebElement ProductCounter => _driver.FindElement(By.ClassName("ajax_cart_no_product"));
        public IWebElement RemoveProduct => _driver.FindElement(By.XPath("//a[@title='Delete']"));
        public IWebElement CartProductLink => _driver.FindElement(By.LinkText("Faded Short Sleeve T-shirts"));

        public void WaitForUpdate()
        {
            _driverWait.Until(value => ProductCounter.Displayed == true);
        }
    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace OnlineStoreAutomationTestPageModel.Page
{
    public class MainPage
    {
        public IWebDriver _driver;
        public WebDriverWait _driverWait;
        public Actions action;
        public string ProductName;

        public MainPage(IWebDriver driver)
        {
            _driver = driver;
            _driverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(15));
            action = new Actions(_driver);
        }

        public IWebElement SearchField => _driver.FindElement(By.Name("search_query"));
        public IWebElement SearchButton => _driver.FindElement(By.Name("submit_search"));
    }
}

﻿using NUnit.Framework;

namespace OnlineStoreAutomationTestPageModel
{
    public class TestSendToFriend : MainTest
    {
        [Test]
        public void GIVEN_FriendNameAndEmail_WHEN_SendingProdctToAFriend_THEN_SendToAFriend()
        {
            var productName = "Faded Short Sleeve T-shirts";

            StoreSetup.MainPage.SearchField.SendKeys(productName);
            StoreSetup.MainPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailsPage.SendToFriendLink.Click();
            StoreSetup.ProductDetailsPage.FriendNameInput.SendKeys("senzo");
            StoreSetup.ProductDetailsPage.FriendEmailInput.SendKeys("senzo@gmail.com");
            StoreSetup.ProductDetailsPage.SendEmailButton.Click();

            Assert.IsTrue(StoreSetup.ProductDetailsPage.SuccessModal.Displayed, "Product not sent to a friend");
        }

        [Test]
        public void GIVEN_EmptyNameAndEmail_WHEN_SendingProductToAFriend_THEN_ShowErrorMessage()
        {
            var productName = "Faded Short Sleeve T-shirts";

            StoreSetup.MainPage.SearchField.SendKeys(productName);
            StoreSetup.MainPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailsPage.SendToFriendLink.Click();
            StoreSetup.ProductDetailsPage.SendEmailButton.Click();

            Assert.AreEqual("You did not fill required fields", StoreSetup.ProductDetailsPage.ErrorMessage.Text);
        }

    }
}

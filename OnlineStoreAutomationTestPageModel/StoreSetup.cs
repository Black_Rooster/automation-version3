﻿using OnlineStoreAutomationTestPageModel.Page;
using OpenQA.Selenium;

namespace OnlineStoreAutomationTestPageModel
{
    public static class StoreSetup
    {
        public static CartPage CartPage;
        public static MainPage MainPage;
        public static ProductDetailsPage ProductDetailsPage;
        public static SearchPage SearchPage;


        public static void Initialize(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

            MainPage = new MainPage(driver);
            CartPage = new CartPage(driver);
            ProductDetailsPage = new ProductDetailsPage(driver);
            SearchPage = new SearchPage(driver);
        }
    }
}

﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace OnlineStoreAutomationTestPageModel
{
    [TestFixture]
    public class MainTest
    {
        IWebDriver _driver;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            StoreSetup.Initialize(_driver);
        }

        [TearDown]
        public void Shutdown()
        {
            _driver.Dispose();
        }
    }
}
